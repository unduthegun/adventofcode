#[allow(dead_code)]
const MAIN: &'static str = include_str!("main.rs");

fn main() {
    println!("Day 00; Template; First Star: {:?}", first_star());
    println!("Day 00; Template; Second Star: {:?}", second_star());
}

fn first_star() -> u32 {
    0
}

fn second_star() -> u32 {
    0
}
